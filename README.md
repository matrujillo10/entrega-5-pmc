## Entrega 5 PettyGuardian

El archivo principal del proyecto es `motion_detection.py`. Este arcivo es el encargado de detectar movimiento, reconocer personas dentro del frame con movimiento y activar el video (`video.mp4`) en caso de que se detecte una persona. 

Antes de poder ejecutar este archivo, es necesario contar con el archivo yolov3.weights, que puede ser descargado con el sigueinte comando:

```
wget https://pjreddie.com/media/files/yolov3.weights 
```

Además, se requeire de `python3.6`, `pytorch`, `opencv3`, `numpy`, `scipy`, `matplotlib` y `pandas`.

# Descripción

Inicialmente, el programa carga el modelo y activa la webcam. Posteriormente, se empieza a detectar movimiento dentro de los frames capturados por la webcam. Una vez detectado movimiento, se procesa el último frame para reconocer los objetos que se movieron en el frame, si se reconoce una persona, se activa un video para llamar la atención de los niños, de lo contrario, no se hace nada.

Concretamente, las principales caracteristicas del MVP son detección de movimiento, reconocimiento de objetos y activación automática de algún actuador (En este caso, un video).

De igual forma, también se trabajo en el stremaing desde el servidor hasta la apliación de celular. Para esto está el proyecto `WebCam` en java.

# Key Features

* Detección de peligros en tiempo real
* Alertas a padres sobre los peligros
* Prevención de accidentes automáticamente

# Archivos de la entrega

* [Diseño de ingeniería](diagrama_despliegue.jpg)
* [Nuevas tecnologías](nuevas_tecnologias.pdf)
* [Lean Canvas](lean-canvas.pdf)
* Actualización de tracción: En la carpeta `traccion/`
