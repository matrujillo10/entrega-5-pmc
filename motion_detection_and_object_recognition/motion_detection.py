# -*- coding = utf-8 -*-
from __future__ import division
import subprocess, sys
import time
import torch 
import torch.nn as nn
from torch.autograd import Variable
import numpy as np
import cv2 
from util import *
from darknet import Darknet
from preprocess import prep_image, inp_to_image
import pandas as pd
import random 
import argparse
import pickle as pkl

def prep_image(img, inp_dim):
    """
    Prepare image for inputting to the neural network. 
    
    Returns a Variable 
    """

    orig_im = img
    dim = orig_im.shape[1], orig_im.shape[0]
    img = cv2.resize(orig_im, (inp_dim, inp_dim))
    img_ = img[:,:,::-1].transpose((2,0,1)).copy()
    img_ = torch.from_numpy(img_).float().div(255.0).unsqueeze(0)
    return img_, orig_im, dim

def label(x):
    c1 = tuple(x[1:3].int())
    c2 = tuple(x[3:5].int())
    cls = int(x[-1])
    label = "{0}".format(classes[cls])
    return label


# When program is started
if __name__ == '__main__':
    cfgfile = "cfg/yolov3.cfg"
    weightsfile = "yolov3.weights"
    num_classes = 80

    confidence = 0.4
    nms_thesh = 0.4

    start = 0
    CUDA = torch.cuda.is_available()

    num_classes = 80
    bbox_attrs = 5 + num_classes
    
    model = Darknet(cfgfile)
    model.load_weights(weightsfile)
    
    model.net_info["height"] = "160"
    inp_dim = int(model.net_info["height"])
    
    assert inp_dim % 32 == 0 
    assert inp_dim > 32

    if CUDA:
        model.cuda()
            
    model.eval()

    # Are we finding motion or tracking
    status = 'motion'
    reproduciendo = False
    opener = "open" if sys.platform == "darwin" else "xdg-open"
    # How long have we been tracking
    idle_time = 0

    # Background for motion detection
    back = None
    # An MIL tracker for when we find motion
    # tracker = cv2.TrackerMIL_create()

    # Webcam footage (or video)
    video = cv2.VideoCapture(0)

    # LOOP
    while True:
        # Check first frame
        ok, frame = video.read()

        # Grayscale footage
        gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)
        # Blur footage to prevent artifacts
        gray = cv2.GaussianBlur(gray,(21,21),0)

        # Check for background
        if back is None:
            # Set background to current frame
            back = gray

        if status == 'motion':
            # Difference between current frame and background
            frame_delta = cv2.absdiff(back,gray)
            # Create a threshold to exclude minute movements
            thresh = cv2.threshold(frame_delta,25,255,cv2.THRESH_BINARY)[1]

            #Dialate threshold to further reduce error
            thresh = cv2.dilate(thresh,None,iterations=2)
            # Check for contours in our threshold
            _,cnts,hierarchy = cv2.findContours(thresh,cv2.RETR_EXTERNAL,cv2.CHAIN_APPROX_SIMPLE)


            # Check each contour
            if len(cnts) != 0:
                # If the contour is big enough

                # Set largest contour to first contour
                largest = 0

                # For each contour
                for i in range(len(cnts)):
                    # If this contour is larger than the largest
                    if i != 0 & int(cv2.contourArea(cnts[i])) > int(cv2.contourArea(cnts[largest])):
                        # This contour is the largest
                        largest = i

                if cv2.contourArea(cnts[largest]) > 3000:
                    # Create a bounding box for our contour
                    (x,y,w,h) = cv2.boundingRect(cnts[0])
                    # Convert from float to int, and scale up our boudning box
                    (x,y,w,h) = (int(x),int(y),int(w),int(h))
                    # Initialize tracker
                    bbox = (x,y,w,h)
                    # ok = tracker.init(frame, bbox)
                    # Switch from finding motion to tracking
                    # status = 'tracking'
                    print('Hay movimiento!')
                    if not reproduciendo:
                        img, orig_im, dim = prep_image(frame, inp_dim)
                        # im_dim = torch.FloatTensor(dim).repeat(1,2)                        
                        
                        if CUDA:
                            im_dim = im_dim.cuda()
                            img = img.cuda()
                        
                        output = model(Variable(img), CUDA)
                        output = write_results(output, confidence, num_classes, nms = True, nms_conf = nms_thesh)

                        if type(output) == int:
                            #print("FPS of the video is {:5.2f}".format( frames / (time.time() - start)))
                            #cv2.imshow("frame", orig_im)
                            key = cv2.waitKey(1)
                            if key & 0xFF == ord('q'):
                                break
                            continue
                                            
                        output[:,1:5] = torch.clamp(output[:,1:5], 0.0, float(inp_dim))/inp_dim
                        
                        output[:,[1,3]] *= frame.shape[1]
                        output[:,[2,4]] *= frame.shape[0]

                        classes = load_classes('data/coco.names')
                        colors = pkl.load(open("pallete", "rb"))
                        

                        l = list(map(lambda x: label(x), output))

                        if 'person' in l:
                            print('Hay una persona!')
                            subprocess.call([opener, "video.mp4"])
                            reproduciendo = True
                else:
                    reproduciendo = False


        # Show our webcam
        cv2.imshow("Camera",frame)


        # If we have been tracking for more than a few seconds
        if idle_time >= 30:
            # Reset to motion
            status = 'motion'
            # Reset timer
            idle_time = 0

            # Reset background, frame, and tracker
            back = None
            # tracker = None
            ok = None

            # Recreate tracker
            # tracker = cv2.TrackerMIL_create()


        # Incriment timer
        idle_time += 1

        # Check if we've quit
        if cv2.waitKey(1) & 0xFF == ord("q") or cv2.getWindowProperty('Camera',0) == -1:
            break

#QUIT
video.release()
cv2.destroyAllWindows()